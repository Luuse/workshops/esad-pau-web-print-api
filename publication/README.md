# Web To Print - Survival Kit

This survival kit, allows you to make a web-to-print edition on any environment.
Two possibilities are there, either to work with a `http` server or directly in local `file`.
## Start Server

VSCode plugin -> [liveServer](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)  
Php server -> `php -S localhost:5000`  
Python server -> `python -m http.server 5000`  

```
├── assets
│   ├── css
│   │   ├── fonts.css # import fonts @font-face
│   │   ├── layout.css # define format variables 
│   │   ├── regions.css # define the flows 
│   │   └── style.css 
│   ├── fonts
│   │   ├── CenturySchL-Ital.otf
│   │   └── CenturySchL-Roma.otf
│   └── js
├── content # content file html 
│   ├── chapter-1.html
│   └── chapter-2.html
├── Layouts 
│   └── chapter-1/ # article layout  
├── index.html
├── lib
│   ├── cssregions.js # Polyfill css regions
│   └── web-to-print-survival-kit.js

```
