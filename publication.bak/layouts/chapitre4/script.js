let contener_img = document.querySelectorAll('.image-contener');
let r;
let g;
let b;
let rgbListe = [];

for (let x = 0; x < 8; x++) {
    r = Math.floor(100 + (Math.random() * 200));
    g = Math.floor(100 + (Math.random() * 200));
    b = Math.floor(100 + (Math.random() * 200));
    rgbListe[x] = "rgb(" + r + "," + g + "," + b + ")"; 
}

contener_img.forEach(image => {
    image.style.backgroundImage = "linear-gradient(to right, "+ rgbListe[0] + "," + rgbListe[1] + "," + rgbListe[2] + "," + rgbListe[3] + "," + rgbListe[4] + "," + rgbListe[5] + "," + rgbListe[6] + "," + rgbListe[7] + ")";
    image.style.backgroundPosition = image.dataset.bgposition + " 240mm";
    // console.log(image.style.backgroundPosition);
});






async function getQuote() {
    let response = await fetch(
        "https://api.windy.com/api/webcams/v2/list/country=FR/property=hd/?key=ukOmIDxh0jhjhXCBUyjoYY3hkxNZqjEg&show=webcams:image,location;categories&limit=15"
        // "https://api.windy.com/api/webcams/v2/list/?key=ukOmIDxh0jhjhXCBUyjoYY3hkxNZqjEg&country=FR"
    );
    let data = await response.json();
    return data;
}

    getQuote().then((data) => {  
        // console.log(data);
        console.log(data);

        let dt = new Date();
        let hour = dt.getHours();
        let hour2 = dt.getHours();
        let minute = dt.getMinutes();
        
        let img0 = document.querySelector("#img0");
        let info0 = document.querySelector("#info0");
        let imageUrl0 = data.result.webcams[0].image.current.preview;
        img0.src = imageUrl0;
        info0.innerHTML = data.result.webcams[0].location.latitude + ", " + data.result.webcams[0].location.longitude + ",<br>" + hour + "h" + minute;

        let img1 = document.querySelector("#img1");
        let info1 = document.querySelector("#info1");
        let imageUrl1 = data.result.webcams[1].image.current.preview;
        img1.src = imageUrl1;
        info1.innerHTML = data.result.webcams[1].location.latitude + ", " + data.result.webcams[1].location.longitude + ",<br>" + hour + "h" + minute;

        let img2 = document.querySelector("#img2");
        let info2 = document.querySelector("#info2");
        let imageUrl2 = data.result.webcams[2].image.current.preview;
        img2.src = imageUrl2;
        info2.innerHTML = data.result.webcams[2].location.latitude + ", " + data.result.webcams[2].location.longitude + ",<br>" + hour + "h" + minute;

        let img3 = document.querySelector("#img3");
        let info3 = document.querySelector("#info3");
        let imageUrl3 = data.result.webcams[3].image.current.preview;
        img3.src = imageUrl3;
        info3.innerHTML = data.result.webcams[3].location.latitude + ", " + data.result.webcams[3].location.longitude + ",<br>" + hour + "h" + minute;

        let img4 = document.querySelector("#img4");
        let info4 = document.querySelector("#info4");
        let imageUrl4 = data.result.webcams[4].image.current.preview;
        img4.src = imageUrl4;
        info4.innerHTML = data.result.webcams[4].location.latitude + ", " + data.result.webcams[4].location.longitude + ",<br>" + hour + "h" + minute;

        let img5 = document.querySelector("#img5");
        let info5 = document.querySelector("#info5");
        let imageUrl5 = data.result.webcams[5].image.current.preview;
        img5.src = imageUrl5;
        info5.innerHTML = data.result.webcams[5].location.latitude + ", " + data.result.webcams[5].location.longitude + ",<br>" + hour + "h" + minute;

        let img6 = document.querySelector("#img6");
        let info6 = document.querySelector("#info6");
        let imageUrl6 = data.result.webcams[6].image.current.preview;
        img6.src = imageUrl6;
        info6.innerHTML = data.result.webcams[6].location.latitude + ", " + data.result.webcams[6].location.longitude + ",<br>" + hour + "h" + minute;
    }); 
