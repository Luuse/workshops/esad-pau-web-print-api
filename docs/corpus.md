---
title: corpus
slug: corpus
---
Liste  

* *Le bruit social, porteur d'information* - Gilbert Simondon - Communication et information - les Éditions de la transparence.  
* *Le tout confort télématique* - Ugo La Pietra - 1983 
* *Utilisateur complet de Turing* - Olia Lialina - 2013
* *Liquider l'utilisateur* - Silvio Lorusso - 2022 - Thèque
* *La synthèse du visage* - Vincent Duché - Azimut 48/49 - 2018 
* *Cybernétique et société - chapitre : Communication, secret et politique sociale* - Nobert Wiener - 1950
