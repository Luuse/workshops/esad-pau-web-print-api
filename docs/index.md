---
title: introduction
slug: introduction
---

Le web et ses données peuvent-ils aider à l’étude et l’augmentation d’un corpus de texte&nbsp;? Durant ce workshop nous allons collectivement penser, designer et imprimer un projet éditorial. Publication qui aura comme particularité ses moyens de production, les langages web. Si nous partons de l’hypothèse que ces langages sont déjà des moyens de mise en page, nous serons amenés à imaginer ce que le web peut procurer de nouveau. Quelles nouvelles formes de relations entre les contenus ? Quelles conditions de lecture ? Quelles nouvelles esthétiques? Sur cette volonté, nous allons explorer des API (application programming interface), et plus particulièrement celles d’accès à des bases de données.

Pad -> [https://annuel2.framapad.org/p/web-api-print-esad](https://annuel2.framapad.org/p/web-api-print-esad)
