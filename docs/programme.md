---
title: programme
slug: programme
---
PRÉVISIONNEL   

<br>
## <u>Lundi</u>  
<br>

### __matin
* Présentations et tour de table. 
* Quels sont nos relations au numérique ? 
* Études de cas d'une dizaine d'objets graphiques imprimés et réalisés par des langages web.
* Rappel:
    * Brève histoire du web (web != internet, naissance du protocole HTTP, exemples de langages.)
    * (Langages serveur / langages client peut-être).
    * Revoir les fondamentaux de l'HTML et du CSS
        * Les balises, leurs fonctions et leurs d'attributs
        * Le CSS, les classes, le ids, les propriétés.

### __après midi
* Exploration des contenus apportés.
	* Arpentage et restitution de lecture.
	* Selections des textes et agencement. 
* Conditions de travail. Temps et gourvernance.
* Conditions de la production. Coût monétaire, matériel et empreinte carbone.
* Comment allons nous rendre public ce travail ? 


## Mardi 
### __matin

### __après midi
## Mercredi
### __matin
### __après midi
## Jeudi
### __matin
### __après midi
## Vendredi 
### __matin
### __après midi
